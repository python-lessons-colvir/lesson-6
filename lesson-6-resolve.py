import json
# help vars
select = "SELECT"
filter_teg = "WHERE"
sort_teg = "ORDER BY"
text_transformation_teg = "TEXT_TRANSFORMATION"
help_methods = []

def write_to_file(string, method_file):
    # print(string)
    # print(method_file)
    with open("result_pars_json", method_file) as result_file:
        result_file.write(string)

with open("request-data.json", "r") as file:
    data = json.load(file)
    data2 = data["nodes"]
    data3 = data["edges"]

    # input function and create output
    # SELECT `id`, `name`, `age` FROM `users`
    def select_input(key_method):
        letter = key_method["key"]
        obj = key_method["transformObject"]["tableName"]
        obj_value = data2[0]["transformObject"]["fields"]
        listToStr = ' '.join([str('`' + elem + '`'+",") for elem in obj_value])[:-1]
        result = f"WITH {letter} as (\n		SELECT {listToStr} FROM `{obj}`\n	),\n"
        write_to_file(result, "w")
        return listToStr

    for key_method in data2:
        # print(key_method["type"])
        if key_method["type"] == "INPUT":
            # print(key_method["type"])
            result_input = select_input(key_method)

    # filter function and create output
    # SELECT `id`, `name`, `age` FROM `A` WHERE `age` > 18
    def select_filter(key_method, result_input, from_value):
        to_value = key_method["key"]
        # age
        variable_field_name = key_method["transformObject"]["variable_field_name"]
        operators = key_method["transformObject"]["operations"][0]
        # >
        operator = operators["operator"]
        # 18
        value = operators["value"]
        result = f"WITH {to_value} as (\n		SELECT {result_input} FROM `{from_value}` {filter_teg} `{variable_field_name}` {operator} {value}\n	),\n"
        write_to_file(result, "a")

    # sort function and create output
    # SELECT `id`, `name`, `age` FROM `B` ORDER BY `age`, `name` DESC
    def select_sort(key_method, result_input, to_value):
        from_value = key_method["key"]
        transformObject = key_method["transformObject"]
        target_array = []
        order_array = []
        for obj in transformObject:
            target_array.append(obj["target"])
            order_array.append(obj["order"])
        target = ' '.join([str('`' + elem + '`'+',') for elem in target_array])[:-1]
        order = order_array[0]
        result = f"WITH {from_value} as (\n		SELECT {result_input} FROM `{to_value}` {sort_teg} {target} {order}\n	),\n"
        write_to_file(result, "a")
        return target


    # text_transformation function and create output
    # SELECT `id`, UPPER(`name`) as `name`, `age` FROM `C`
    def select_text_transformation(key_method, result_input, result_sort, to_value):
        from_value = key_method["key"]
        for tt_param in key_method["transformObject"]:
            tt_method = tt_param["transformation"]
            tt_column = tt_param["column"]
            tt_param_concat = ''.join([str(tt_method + '('+'`' + tt_column + '`' + ')')])
        result = f"WITH {from_value} as (\n		SELECT {result_input[:5]} {tt_param_concat} as {result_sort} FROM `{to_value}`\n	),\n"
        write_to_file(result, "a")

    # select * from function and create output
    # SELECT * FROM `D` limit 100 offset 0
    import numpy as np
    def select_all(key_method, result_input, result_sort, to_value):
        from_value = key_method["key"]
        print(key_method)
        result = key_method["transformObject"].items()
        print(result)
        data = list(result)
        numpyArray = np.array(data)
        print(numpyArray)
        string_met = ''.join([str(' ' + i + ' ') for item in numpyArray for i in item])
        print(string_met)
        result = f"WITH {from_value} as (\n		SELECT * FROM `{to_value}` {string_met} \n	)\nSELECT * FROM {from_value};"
        write_to_file(result, "a")
    

    def iterate_data2(from_value, to_value):
        for key_method in data2:
            if key_method["key"] == to_value:
                # print(key_method["type"]) 
                #FILTER SORT TEXT_TRANSFORMATION OUTPUT
                if key_method["type"] == "FILTER":
                    select_filter(key_method, result_input, from_value)
                elif key_method["type"] == "SORT":
                    global result_sort
                    result_sort = select_sort(key_method, result_input, from_value)
                elif key_method["type"] == "TEXT_TRANSFORMATION":
                    select_text_transformation(key_method, result_input, result_sort, from_value)
                elif key_method["type"] == "OUTPUT":
                    select_all(key_method, result_input, result_sort, from_value)



    def total_count():
        for edges_key in data3:
            # print(edges_key)
            from_value = edges_key["from"]
            to_value = edges_key["to"]

            iterate_data2(from_value, to_value)


    total_count()




########## task 2 ####################
# Print dummy data
# print("Name:", fake.name())
# print("Email:", fake.email())
# print("Address:", fake.address())
# print("Country:", fake.country())
# print("URL:", fake.url())
# fake.phone_number()
# fake.random_number(digits=5)
# age = int(f.random_number(digits=2))+10

import csv
from multiprocessing.sharedctypes import Value
from faker import Faker
data = []

# create faker data function
def fake_data():
    f = Faker("ru_RU")
    name = {"ФИО" : f.name(), "возраст" : int(f.random_number(digits=2))+10, "адрес" : f.address(), "email" : f.email()}
    return name


def create_csv(x):
    for i in range(500):
        data.append(fake_data())
        
    #create csv file
    person_info = ['ФИО', 'возраст', 'адрес', 'email']
    writer_csv = csv.DictWriter(x, fieldnames=person_info)
    writer_csv.writeheader()
    writer_csv.writerows(data)
    return x


#create file list_of_names and write fake data in it
def open_file(x, attr):
    with open("list_of_names.csv", str(attr)) as file_names:
        return x(file_names)


open_file(create_csv, "w")


arr = []
py = csv.DictReader(open("list_of_names.csv"))


for row in py:
    name = row.get("ФИО").split()[1]
    arr.append(name)
arr_sort = sorted(arr)
arr_len = len(arr)

count = 0
name_often = ""
count_2 = 0
name2 = ""

for index,elem in enumerate(arr_sort):
    if index+2 > arr_len:
        print("break")
        continue
    if arr_sort[index] == arr_sort[index+1]:
        count_2 += 1
        name2 = elem
    elif arr_sort[index] != arr_sort[index+1]:
        if count < count_2:
            count = count_2
            name_often = name2
            count_2 = 0
            name2 = ""
        else:
            count_2 = 0
            name2 = ""
    else:
        print("else")


print("Often name is " + name_often + " and count " + str(count) + " times")